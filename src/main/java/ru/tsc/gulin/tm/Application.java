package ru.tsc.gulin.tm;

import ru.tsc.gulin.tm.component.Bootstrap;

public final class Application {

    public static void main(String[] args){
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
