package ru.tsc.gulin.tm.api.repository;

import ru.tsc.gulin.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    Task remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    Task create(String name);

    Task create(String name, String description);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task add(Task task);

    void clear();

}
