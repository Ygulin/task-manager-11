package ru.tsc.gulin.tm.api.service;

import ru.tsc.gulin.tm.model.Task;

import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    Task remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    Task create(String name);

    Task create(String name, String description);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task add(Task task);

    void clear();

}
